#!/usr/bin/env python3
# Helpful LXD creation bits

from time import sleep
import pylxd
import sys

client = pylxd.Client()

setup_container_name = "sp-setup"
image_prefix = "sp-201912-"
release = "bionic"
image_name = image_prefix + release


def yes_no(answer):
    # From: https://overlaid.net/2016/02/09/simple-yes-no-function-in-python/
    yes = set(['yes', 'y', 'ye'])
    no = set(['no', 'n'])
    while True:
        choice = input(answer).lower()
        if choice in yes:
            return True
        elif choice in no:
            return False
        else:
            print("Please respond with 'yes' or 'no'\n")


def CreateAndStartContainer(config):
    container = client.containers.create(config, wait=True)
    StartContainer(container)
    return container


def InitContainer(container_name):
    if client.containers.exists(container_name):
        print("Existing container found, using: " + container_name)
        container = client.containers.get(container_name)
    else:
        print("No container found, creating a new one: " + container_name)
        containerconfig = {'name': container_name, 'source':
                           {'type': 'image', 'alias': image_name}}
        container = CreateAndStartContainer(containerconfig)

    return container


def StartContainer(container):
    if container.status != "Running":
        print("\nStarting container: " + container.name, end='', flush=True)
        container.start(wait=True)
    else:
        print("The container is already running")
    # Blah
    # systemd goes into degraded state in 18.04 so we can't use exit codes
    # If you switch to 19.04 you can use exit code of 0, but 19.10 broke it
    sleep(5)
    running = ""
    while not running == "degraded\n" or running == "running\n":
        sleep(1)
        print(".", end='', flush=True)
        running = container.execute(['systemctl', 'is-system-running']).stdout
    print("\nContainer started")
    return


def CleanOldImages():
    # Remove old images that are part of this list
    try:
        lxd_image = client.images.get_by_alias("sp-base-image")
        lxd_image.delete(wait=True)
        print("..deleting existing image")
    except pylxd.exceptions.NotFound:
        return
    return


def CheckImages():
    # Check to see if the Scout Plane image exists, if not, create it
    try:
        client.images.get_by_alias(image_name)
    except pylxd.exceptions.NotFound:
        print("No image exists, let's make one!\n")
        CheckContainer(setup_container_name)
        createlxdimage()
        print("New image created!\n")
    return


def CheckContainer(container_name):
    if client.containers.exists(container_name):
        if yes_no("Do you want to delete the existing container? [Yes/No] "):
            print("Deleting container: " + container_name)
            container = client.containers.get(container_name)
            if container.status != "Stopped":
                container.stop(wait=True)
                container.delete(wait=True)
        else:
            print("You have chosen to exit")
            sys.exit()


def createlxdimage():
    config = {
        'name': setup_container_name,
        'source': {
            'type': 'image',
            "mode": "pull",
            "server": "https://cloud-images.ubuntu.com/daily/",
            "protocol": "simplestreams",
            'alias':  release + '/amd64'
        }
    }

    container = CreateAndStartContainer(config)

    print("Configuring container image for Scout Plane")
    print("...." + str(
       container.execute(['apt-get', 'update']).exit_code))
    print("...." + str(
       container.execute([
            'git',
            'clone',
            '-b',
            'distros',
            'https://github.com/BryanQuigley/xsos.git',
            '/home/ubuntu/xsos']).exit_code))
    print("...." + str(
       container.execute([
            'apt-get',
            'purge',
            'rsyslog',
            'lxd',
            'lxd-client',
            'snapd',
            '--yes',
            '--quiet',
        ]).exit_code))
    print("...." + str(container.execute([
        'apt-get',
        'install',
        'tree',
        'lnav',
        '--yes',
        '--quiet',
       ]).exit_code))
    print("Completed configuring the container image.")

    container.stop(wait=True)

    print("Now the custom image will be published.")
    image = container.publish(wait=True)
    image.add_alias(name=image_name, description="ScoutPlane image")

    print("Cleaning up.")
    container.delete(wait=True)
    return


if __name__ == "__main__":
    try:
        lxd_image = client.images.get_by_alias(image_name)
        lxd_image.delete(wait=True)
        print("..deleting existing image")
    except pylxd.exceptions.NotFound:
        print("..No Image previously setup")
    createlxdimage()


# vim: set et ts=4 sw=4 :
