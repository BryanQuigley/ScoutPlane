#!/usr/bin/env python3
import paramiko
import os
import argparse
import sys
import re
import configparser
import stat
from pathlib import Path
import lxdhelper
import pylxd
client = pylxd.Client()

config_file = str(Path.home()) + '/.config/scoutplane.ini'
CASENUM_REGEX = r'(?<!\d)\d{6,8}(?!\d)'

config = configparser.ConfigParser()


def GetHumanReadable(size, precision=2):
    # Credit to Pavan Gupta https://stackoverflow.com/questions/5194057/
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB']
    suffixIndex = 0
    while size > 1024 and suffixIndex < 4:
        suffixIndex += 1
        size = size/1024.0
    return "%.*f%s" % (precision, size, suffixes[suffixIndex])


def VerifyMD5Sum(container, cont_filename_tar_dir, filename, md5sum):

    print("Verify: \tStarting")
    verify = container.execute(
            ['md5sum', cont_filename_tar_dir + filename]).stdout[0:32]
    if str(verify) == str(md5sum):
        print("Verify: \tComplete")
    else:
        print("Verify: \tFAILED")


def FiletoContainer(
        osre, container, cont_filename_tar_dir, filename, directory):
    filestat = osre.stat(directory + "/" + filename)

    if args.local:
        fileptr = open(directory + "/" + filename, 'rb').read()
    else:
        fileptr = osre.open(directory + "/" + filename, mode='r')
        fileptr.prefetch(filestat.st_size)

    print("Copy: \t " + directory + "/" + filename)
    print("Size: \t " + GetHumanReadable(filestat.st_size))
    container.files.put(cont_filename_tar_dir + filename, fileptr)

    try:
        if args.local:
            md5sum = open(directory + "/" + filename + ".md5").readline()[0:32]
        else:
            md5sum = osre.open(
                     directory + "/" + filename + ".md5").readline()[0:32]
        VerifyMD5Sum(container, cont_filename_tar_dir, filename, md5sum)
    except OSError:
        print_verbose("Verify: \tno md5sum exist at source")
        return


def ImportSshKey(container, lp_id):
    container.execute([
        'ssh-import-id',
        lp_id,
    ])


def Extract(container, cont_filename_tar_dir, filename):
    print("Extract: \tStarting")
    extraction = container.execute([
        'tar',
        'xfa',
        cont_filename_tar_dir + filename,
        '-C',
        cont_filename_tar_dir,
        '--exclude=dev/null',
        '--no-same-owner',
        '--strip-components=1',
    ])
    if(extraction[0]) == 0:
        print("Extract: \tComplete " + cont_filename_tar_dir)
    else:
        print("Extract: \tPossible error, analysis stopping")
        print(extraction)
        sys.exit(2)  # Give up..


def CheckConfigFile(path):
    # Create a config file if it doesn't exist
    config_dir = os.path.dirname(path)
    if not os.path.exists(config_dir):
        os.makedirs(config_dir)
    if not os.path.exists(path):
        print(path)
        print("No configuration file detected! Let's make one!\n")
        print("\nPlease Enter a server name:")
        server_name = input()
        print("\nNow enter your User name for the file server:")
        user_name = input()
        config['REMOTE'] = {'Server': server_name, 'Username': user_name}
        print("\nPlease provide your Launchpad ID:")
        launchpad_id = input()
        config['USER'] = {'lp_id': launchpad_id}
        config.write(open(path, 'w'))
        print("\nThanks! Now we are ready to continue.\n")


def get_connection(hostname, username):
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    try:
        ssh.connect(hostname, username=username)
    except paramiko.SSHException as e:
        print("Connection Error")
        print(e)
        sys.exit(2)
    except KeyError:
        print("Error: Maybe scoutplane.ini isn't configured correctly?")
        sys.exit(2)
    conn = ssh.open_sftp()
    conn.keep_this = ssh

    return conn


def print_verbose(command):
    if args.verbose:
        print(command)
    else:
        command


parser = argparse.ArgumentParser()
parser.add_argument(
    "path", help="full remote file path aka "
    "/customers/ABCinc/10101/sosreport-xyz.tar.xz")
parser.add_argument(
    "--config", help="Specify a config file for ScoutPlane",
    default=config_file)
parser.add_argument(
    "-c", "--containername", help="specify container name to override "
    "autodetected case number from directory name, recommended if using local")
parser.add_argument(
    "--id", help="Specify the Launchpad ID you would like to use for "
    "importing ssh keys. If no ID is provided, the ID in the config will be "
    "used.")
parser.add_argument(
    "-v", "--verbose", help="print more stuff", action="store_true")
location = parser.add_mutually_exclusive_group()
location.add_argument(
    "-l", "--local", help="use local filesystem", action="store_true")
location.add_argument(
    "-r", "--remote",
    help="[default] use remote filesystem",
    action="store_true",
)
args = parser.parse_args()

CheckConfigFile(args.config)
config.sections()
config.read(args.config)
lxdhelper.CleanOldImages()
lxdhelper.CheckImages()
hostname = config['REMOTE']['Server']
username = config['REMOTE']['Username']
lp_id = config['USER']['lp_id']

directory, filename = os.path.split(args.path)
filenames = []

if args.local:
    import os as osre  # os or remote
else:
    osre = get_connection(hostname, username)

# Does File or directory actually exit?
try:
    filestat = osre.stat(args.path)
except FileNotFoundError:
    print("Error: File not found")
    sys.exit(2)

if stat.S_ISDIR(filestat.st_mode) and filename == '':
    filenames = osre.listdir(args.path)
    print("Target: \tDirectory")
elif stat.S_ISREG(filestat.st_mode) and filename != '':
    print("Target: \tFile")
    filenames.insert(0, filename)
else:
    print("Error: File does not exist or mismatch from what we expect")
    print(" target filename:\t" + filename)
    print(" target directory:\t " + directory)
    print(" is target file:\t" + str(stat.S_ISREG(filestat.st_mode)))
    print(" is target directory:\t" + str(stat.S_ISDIR(filestat.st_mode)))
    sys.exit(2)

# Determine Container Name
if args.containername:
    if client.containers.exists(args.containername):
        container_name = args.containername
    else:
        container_name = "sp-" + args.containername
else:
    # If we can find a case number use that
    match = re.findall(CASENUM_REGEX, str(args.path))
    if match:
        casenumber = match[0]
    # Otherwise use the dirname fallback everywhere
    else:
        try:
            dirname = directory.rsplit("/", 1)
            casenumber = dirname[1]
        except IndexError:
            print("Unable to case number, please specify manually")
            sys.exit(2)
    container_name = "sp-" + casenumber

container = lxdhelper.InitContainer(container_name)

# Import SSH Key
if args.id:
    ImportSshKey(container, args.id)
else:
    ImportSshKey(container, lp_id)

for filename in filenames:
    try:
        filestat = osre.stat(directory + "/" + filename)
        if stat.S_ISDIR(filestat.st_mode):
            print("Skipping directory " + directory + "/" + filename)
            continue
    except FileNotFoundError:
        print("Error: File doesn't exist on local machine")
        sys.exit(2)

    if ".md5" in filename:
        continue

    # Create working directory just for this file
    cont_filename_tar_dir = '/home/ubuntu/D' + filename + '/'
    print_verbose(container.execute(['mkdir', cont_filename_tar_dir]))

    FiletoContainer(
            osre, container, cont_filename_tar_dir, filename, directory)

    # If it's a compresed tarball, let's extract it!
    if ".tar." in filename:
        Extract(container, cont_filename_tar_dir, filename)

    print("\n")

if not args.local:
    osre.close()

print("Potentially helpful things to do next:")
print("lxc exec " + container_name + " bash")
print("cd " + cont_filename_tar_dir)

# vim: set et ts=4 sw=4 :
